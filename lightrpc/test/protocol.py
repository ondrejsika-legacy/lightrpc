import os
import sys
sys.path.append(os.path.abspath(os.path.join(__file__, '..', '..', '..')))

from lightrpc.lightrpc import LightRPC


def _assert(a, b):
    if a == b:
        print 'OK'
    else:
        assert a == b


class TestProtocol(LightRPC):
    @LightRPC.register('sum')
    def sum(self, data, callback):
        callback('sum_response', (data, sum(data)))

    @LightRPC.register('sum_response')
    def sum_response(self, data, callback):
        _assert(sum(data[0]), data[1])