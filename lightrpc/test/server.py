from twisted.internet import protocol, reactor, endpoints
from protocol import TestProtocol


class Factory(protocol.Factory):
    def buildProtocol(self, addr):
        return TestProtocol()


endpoints.serverFromString(reactor, 'tcp:9999').listen(Factory())
reactor.run()