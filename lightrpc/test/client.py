from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol

from protocol import TestProtocol


def _process(p):
   p.call('sum', [1, 2, 3, 4])


point = TCP4ClientEndpoint(reactor, '127.0.0.1', 9999)
conn = connectProtocol(point, TestProtocol())
conn.addCallback(_process)
reactor.run()