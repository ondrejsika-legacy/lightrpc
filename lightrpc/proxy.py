# coding: utf-8

from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

from .client import build_raw_request


class LightRPCProxy(Resource):
    isLeaf = True

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def render_POST(self, http_request):
        def callback(data):
            print '> proxy >', data
            http_request.write(data)
            http_request.finish()
        request = http_request.content.read()
        build_raw_request(self.host, self.port, callback, [request])
        return NOT_DONE_YET

