# coding: utf-8

import json

from twisted.internet import protocol


class LightRPC(protocol.Protocol):
    """
    Very light json based RPC
    =========================

    Protocol specs
    --------------

    Request

        {
            "name": ...,  # str name of method
            "data": ...,  # json object
        }


    example of subclass with definition RPC method
    ----------------------------------------------

        class MyRPC(LightRPC):
            @LightRPC.regiter('sum')
            def sum(self, data, callback):
                result = sum(data)
                callback('sum_resp', result)

            @LightRPC.regiter('sum_resp')
            def sum_resp(self, data, callback):
                print 'sum: %s' % data
    """

    _methods = {}

    @staticmethod
    def serialize(name, data):
        return json.dumps({
            'name': name,
            'data': data,
        })

    @staticmethod
    def deserialize(json_data):
        data = json.loads(json_data)
        name = data['name']
        data = data['data']
        return name, data

    def call(self, name, data):
        self.transport.write(LightRPC.serialize(name, data)+'\n')

    def dataReceived(self, payload):
        name, data = LightRPC.deserialize(payload)
        self._methods[name](self, data, self.call)

    @classmethod
    def register(cls, name):
        def decorator(func):
            def wrap(self, arg, callback):
                return func(self, arg, callback)
            if name in cls._methods:
                raise cls.LightRPCError('Name "%s" is already registred' % name)
            cls._methods[name] = wrap
            return wrap
        return decorator

    class LightRPCError(Exception):
        pass

