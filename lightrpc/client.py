# coding: utf-8

from twisted.internet.protocol import ClientFactory
from twisted.internet import reactor
from twisted.protocols.basic import LineReceiver

from lightrpc import LightRPC


def _build_factory(protocol):
    factory = ClientFactory()
    factory.protocol = protocol
    return factory


def _build_client(protocol, requests):
    print(344343)
    print(requests)
    class LightRPCClient(protocol):
        active_requests = 0

        def connectionMade(self):
            for request in requests:
                print '>', request
                self.transport.write(request+'\n')
                self.active_requests += 1

        def dataReceived(self, payload):
            print '>>', payload
            super(protocol, self).dataRecieved(payload)
            self.active_requests += 1
            if self.active_requests == 0:
                self.transport.loseConnection()

    return LightRPCClient


def build_raw_request(host, port, protocol, requests):
    reactor.connectTCP(host, port, _build_factory(_build_client(protocol, requests)))


def build_request(host, port, protocol, name, data):
    build_raw_request(host, port, protocol, [LightRPC.serialize(name, data)])

